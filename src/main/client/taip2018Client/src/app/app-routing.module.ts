import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {VoteFormComponent} from './components/vote-form/vote-form.component';

const routes: Routes = [
{ path: 'login', component: LoginComponent },
{ path: 'register', component: RegisterComponent },
{ path: 'change-password', component: ChangePasswordComponent },
{ path: 'reset-password', component: ResetPasswordComponent },
{ path: 'vote-form', component: VoteFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
