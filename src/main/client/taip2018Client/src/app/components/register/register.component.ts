import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {RegisterService} from '../../services/register/register.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
form: FormGroup;
constructor(
    private formBuilder: FormBuilder, private _registerService: RegisterService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      fullName: '',
      email: '',
      password: '',
      verifyPassword: ''
    });
  }
submit() {
      console.log(this.form.value);
      this._registerService.register(this.form.value)
        .subscribe(
          data => console.log('Success!', data),
          error => console.log('Error!', error)
        )
    }

}
