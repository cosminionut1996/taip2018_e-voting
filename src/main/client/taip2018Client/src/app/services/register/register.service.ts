import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  _url= '';
  constructor(private _http: HttpClient) { }

  register(registerForm : any){
    return this._http.post<any>(this._url, registerForm);
}
}
