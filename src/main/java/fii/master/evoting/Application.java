package fii.master.evoting;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.model.Voter;
import fii.master.evoting.repository.BallotRepository;
import fii.master.evoting.repository.CandidateRepository;
import fii.master.evoting.repository.VoterRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner init(VoterRepository voterRepository, CandidateRepository candidateRepository){
        return (args) ->{
            };
    }
}
