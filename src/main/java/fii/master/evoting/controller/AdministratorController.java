package fii.master.evoting.controller;

import fii.master.evoting.dto.CandidateDto;
import fii.master.evoting.model.Candidate;
import fii.master.evoting.service.Implementations.AdministratorService;
import fii.master.evoting.service.Implementations.CandidatesService;
import fii.master.evoting.service.Interfaces.IAdministratorService;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AdministratorController {

    private IAdministratorService administratorService;

    @Autowired
    public AdministratorController(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    @GetMapping("/administrator/post")
    public void AddCandidate(CandidateDto candidate){
        if (candidate != null)
        {
            Candidate object = new Candidate();
            object.setName(candidate.getName());

            administratorService.AddCandidate(object);
        };
    }

}
