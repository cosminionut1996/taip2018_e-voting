package fii.master.evoting.controller;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.service.Implementations.CandidatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/candidates")
public class CandidateController {

    private CandidatesService candidatesService;

    @Autowired
    public CandidateController(CandidatesService candidatesService) {
        this.candidatesService = candidatesService;
    }

    @GetMapping("/")
    List<Candidate> getCandidates(){
        return candidatesService.getAllCandidates();
    }

}
