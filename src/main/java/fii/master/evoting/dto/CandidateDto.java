package fii.master.evoting.dto;

public class CandidateDto {
    Long candidateId;
    String name;

    public CandidateDto() {
    }

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
