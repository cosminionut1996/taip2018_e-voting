package fii.master.evoting.mapper;

import fii.master.evoting.dto.CandidateDto;
import fii.master.evoting.model.Candidate;

public class CandidateMapper {
    public CandidateDto map(Candidate candidate){
        CandidateDto dto = new CandidateDto();
        dto.setName(candidate.getName());
        dto.setCandidateId(candidate.getId());

        return dto;
    }
}
