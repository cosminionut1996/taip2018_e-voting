package fii.master.evoting.model;


public class Admin {

    private static Admin instance = null;

    private long g;

    private long q;

    public void setG(long g) {
        this.g = g;
    }

    public long getG() {
        return g;
    }

    public void setQ(long q) {
        this.q = q;
    }

    public long getQ() {
        return q;
    }

    private Admin()
    {
        //createBulletinBoard();
    }

    public static Admin getAdminInstance()
    {
        if(instance == null)
            instance=new Admin();

        return instance;
    }


}


