package fii.master.evoting.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class BulletinBoard
{
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany
    @JoinColumn(name="candidate_id")
    private List<Candidate> candidates;

    public BulletinBoard()
    {

    };

}

