package fii.master.evoting.model;

import javax.persistence.*;

@Entity
public class TallyAuth{

    @Id
    @GeneratedValue
    private Long id;

    private long partialResult;

    private long x;

    private long y;

    private long publicKey;

    private long privateKey;

    public TallyAuth() { }

    public void setPrivateKey(long privateKey)
    {
        this.privateKey=privateKey;
    }

    public long getPrivateKey()
    {
        return privateKey;
    }

    public void setPublicKey(long publicKey)
    {
        this.publicKey=publicKey;
    }

    public long getPublicKey()
    {
        return publicKey;
    }

    public void setPartialResult(long partialResult)
    {
        this.partialResult=partialResult ;
    }

    public long getPartialResult()
    {
        return partialResult;
    }

    public void setX(long x)
    {
        this.x=x;
    }

    public long getX()
    {
        return x;
    }

    public void setY(long y)
    {
        this.y=y;
    }

    public long getY()
    {
        return y;
    }

}