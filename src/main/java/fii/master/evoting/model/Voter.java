package fii.master.evoting.model;

import org.hibernate.annotations.ColumnTransformer;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.*;

@Entity
public class Voter {

    @Id
    @GeneratedValue
    private Long id;

    @ColumnTransformer(
            read =  "pgp_sym_decrypt(cnp, current_setting('encrypt.key'))",
            write = "pgp_sym_encrypt(?, current_setting('encrypt.key')) "
    )
    @Column(columnDefinition = "bytea")
    private String cnp;

    private String email;

    @ColumnTransformer(
            read = "encode(digest(password, 'sha1'), 'hex')",
            write = "digest(?, 'sha1')"
    )
    private String password; // will be changed into a hash later

    private String valueA;
    private String valueB;

    @OneToOne
    @JoinColumn(name = "ballot_id")
    private Ballot ballot;

    public Voter() {
    }

    public Long getId() {
        return id;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public String getValueA() {
        return valueA;
    }

    public void setValueA(String valueA) {
        this.valueA = valueA;
    }

    public String getValueB() {
        return valueB;
    }

    public void setValueB(String valueB) {
        this.valueB = valueB;
    }


    }

