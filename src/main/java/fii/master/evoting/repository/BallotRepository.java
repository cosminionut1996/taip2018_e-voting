package fii.master.evoting.repository;

import fii.master.evoting.model.Ballot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BallotRepository extends JpaRepository<Ballot, Long> {
}
