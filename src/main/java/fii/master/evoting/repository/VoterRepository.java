package fii.master.evoting.repository;

import fii.master.evoting.model.Voter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoterRepository extends JpaRepository<Voter, Long>
{

}
