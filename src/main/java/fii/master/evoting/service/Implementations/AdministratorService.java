package fii.master.evoting.service.Implementations;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.model.Voter;
import fii.master.evoting.service.Interfaces.IAdministratorService;
import fii.master.evoting.service.Interfaces.IAlgorithmService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("administratorService")
public class AdministratorService implements IAdministratorService {
    private IAlgorithmService _algorithmService;
    private String _privateKey;
    private String _publicKey;

    @Override
    public boolean AddCandidate(Candidate candidate) {
        return false;
    }

    @Override
    public boolean AddVoter(Voter voter) {
        return false;
    }

    @Override
    public boolean StartVotingSession(int setTimer) {
        return false;
    }

    private void CalculateResults(List<Integer> ballots)
    {

    }

    @Override
    public boolean GetResults() {
        _algorithmService = new CalculateVotesAlgorithm();
        return false;
    }

    public void GenerateKeys()
    {
        _algorithmService = new ElGamalAlgorithm();
    }

}
