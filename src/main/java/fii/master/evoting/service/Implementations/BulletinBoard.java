package fii.master.evoting.service.Implementations;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.model.Voter;
import fii.master.evoting.service.Interfaces.IBulletinBoard;

import java.util.List;
import java.util.Observable;

public class BulletinBoard extends Observable implements IBulletinBoard {
    private List<Candidate> _candidates;
    private List<Voter> _voters;

    public BulletinBoard() {

    }

    public void NotifyVoters()
    {

    }

    public void SetCandidates(List<Candidate> candidates)
    {

        NotifyVoters();
        DisplayCandidates();
    }

    public void DisplayCandidates()
    {

    }
}
