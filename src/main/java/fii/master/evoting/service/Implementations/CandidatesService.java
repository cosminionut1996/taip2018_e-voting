package fii.master.evoting.service.Implementations;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.repository.CandidateRepository;
import fii.master.evoting.service.Interfaces.ICandidatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatesService implements ICandidatesService {
    private CandidateRepository candidateRepository;

    @Autowired
    public CandidatesService(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public List<Candidate> getAllCandidates() {
        return candidateRepository.findAll();
    }
}
