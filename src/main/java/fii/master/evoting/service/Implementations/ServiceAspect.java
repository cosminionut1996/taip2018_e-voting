package fii.master.evoting.service.Implementations;

import fii.master.evoting.service.Interfaces.IVotingService;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class ServiceAspect {
    protected IVotingService _votingService;
    protected static Logger logger = LoggerFactory.getLogger(ServiceAspect.class);
    private Long _clientId;

    public ServiceAspect(IVotingService votingService)
    {
        _votingService = votingService;
    }

    @Pointcut("execution(* fii.master.evoting.*.*(..))")
    private boolean checkStatus(Long id)
    {
        if (_votingService.CheckVoter(id))
        {
            _clientId = id;
            return true;
        }

        return false;
    }

    @Before("checkStatus(Long)")
    public void WriteBeforeCheck()
    {
        logger.info("The voter with id: " + _clientId + " is being checked...");
    }

    @AfterReturning(pointcut = "checkStatus(Long)", returning = "result")
    public void WriteAfterCheck(boolean result)
    {
        logger.info("The check on id: " + _clientId + " was " + result + ". The proper actions are being taken.");
    }

    @Pointcut("execution(* fii.master.evoting.service.Interfaces.IVotingService.VoteCandidate()) && args()")

    @After("execution(* fii.master.evoting.service.Interfaces.IVotingService.VoteCandidate()) && args()")
    public void AfterVoteCandidate()
    {
        logger.info("The voter with id: " + _clientId + "has voted successfully");
    }

    @Pointcut("execution(* fii.master.evoting.service.Interfaces.IStringHasher.hash()) && args()")

    @Before("execution(* fii.master.evoting.service.Interfaces.IStringHasher.hash()) && args()")
    public void BeforeHashingCnp()
    {
        logger.info("Using the given key, the function is hashing for the given text");
    }

    @After("execution(* fii.master.evoting.service.Interfaces.IStringHasher.hash()) && args()")
    public void AfterHashingCnp()
    {
        logger.info("The hashing is done!");
    }
}
