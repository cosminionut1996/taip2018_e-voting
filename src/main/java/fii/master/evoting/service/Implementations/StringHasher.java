package fii.master.evoting.service.Implementations;

import fii.master.evoting.service.Interfaces.IStringHasher;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class StringHasher implements IStringHasher {
    @Override
    public String hash(String plaintext) {
         return BCrypt.hashpw(plaintext, BCrypt.gensalt());
    }
}
