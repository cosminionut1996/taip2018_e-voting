package fii.master.evoting.service.Implementations;

import fii.master.evoting.service.Interfaces.ISystemServices;

public class SystemServices implements ISystemServices {
    private static SystemServices _instance;
    private EmailSender _emailSender;

    private SystemServices()
    {
    }

    public SystemServices GetInstance()
    {
        if (_instance == null) {
            _instance = new SystemServices();
            _emailSender = new EmailSender("username", "password");
        }

        return _instance;
    }

    @Override
    public boolean CheckIfCitizenIsEligible(String cnp) {
       //code comes here
        return true;
    }

}
