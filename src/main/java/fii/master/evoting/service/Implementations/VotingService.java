package fii.master.evoting.service.Implementations;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.model.Voter;
import fii.master.evoting.repository.VoterRepository;
import fii.master.evoting.service.Interfaces.IVotingService;

public class VotingService implements IVotingService {

    protected VoterRepository _repository;

    public VotingService(VoterRepository repository)
    {
        _repository = repository;
    }

    @Override
    public boolean CheckVoter(Long id)
    {
        return _repository.existsById(id);
    }

    @Override
    public boolean VoteCandidate(Candidate candidate) {
        return false;
    }
}
