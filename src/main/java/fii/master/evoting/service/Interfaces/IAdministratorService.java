package fii.master.evoting.service.Interfaces;

import fii.master.evoting.model.Candidate;
import fii.master.evoting.model.Voter;

public interface IAdministratorService {
    boolean AddCandidate(Candidate candidate);
    boolean AddVoter(Voter voter);
    boolean StartVotingSession(int setTimer);
    boolean GetResults();
    void GenerateKeys();
}
