package fii.master.evoting.service.Interfaces;

import fii.master.evoting.model.Candidate;

import java.util.List;

public interface IBulletinBoard {
    void NotifyVoters();
    void SetCandidates(List<Candidate> candidates);
    void DisplayCandidates();
}
