package fii.master.evoting.service.Interfaces;

import fii.master.evoting.model.Candidate;

import java.util.List;

public interface ICandidatesService {
    List<Candidate> getAllCandidates();
}
