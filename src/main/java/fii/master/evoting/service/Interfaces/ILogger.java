package fii.master.evoting.service.Interfaces;

public interface ILogger {
    void WriteMessage(String message);
    void SetLocation(String path);
}
