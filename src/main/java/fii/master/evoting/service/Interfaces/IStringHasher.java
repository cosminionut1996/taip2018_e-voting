package fii.master.evoting.service.Interfaces;

public interface IStringHasher {
    public String hash(String plaintext);
}
