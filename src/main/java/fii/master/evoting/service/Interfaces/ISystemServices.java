package fii.master.evoting.service.Interfaces;

import fii.master.evoting.model.Voter;
import fii.master.evoting.service.Implementations.SystemServices;

public interface ISystemServices {
    SystemServices GetInstance();
    boolean CheckIfCitizenIsEligible(String cnp);
}
