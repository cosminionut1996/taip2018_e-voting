package fii.master.evoting.service.Interfaces;

import java.util.List;

public interface ITallyAuthority {
    void calculateBallotsForX();
    void CalculateBallotsForY();
    List<Integer> GetBallots();
}
