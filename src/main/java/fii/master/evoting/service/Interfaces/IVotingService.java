package fii.master.evoting.service.Interfaces;

import fii.master.evoting.model.Candidate;

public interface IVotingService {
    boolean VoteCandidate(Candidate candidate);
    boolean CheckVoter(Long id);
}
