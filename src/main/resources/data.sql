CREATE EXTENSION IF NOT EXISTS pgcrypto;

delete from candidate;
delete from bulletin_board;
delete from voter;

INSERT INTO public.candidate(id, name) VALUES (1, 'Liberalus');
INSERT INTO public.candidate(id, name) VALUES (2, 'Democratus');

INSERT INTO public.voter(id, cnp, email, password, valuea, valueb, ballot_id)VALUES (3, '123456789', 'george@gmail.com', 'dada', 'xyz', 'oklp', null);
INSERT INTO public.voter(id, cnp, email, password, valuea, valueb, ballot_id)VALUES (4, '864967442', 'cosmina@icloud.com', 'blabla', 'qwt', 'vcxz', null);
INSERT INTO public.voter(id, cnp, email, password, valuea, valueb, ballot_id)VALUES (5, '556780761', 'alex@yahoo.ro', 'hushus', 'xc123', '123ad', null);